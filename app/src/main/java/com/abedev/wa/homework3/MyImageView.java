package com.abedev.wa.homework3;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by home on 27.08.2016.
 */
public class MyImageView extends ImageView {

    public interface SizeChangedListener {
        void onSizeChanged(int w, int h);
    }
    private SizeChangedListener sizeChangedListener = null;

    public MyImageView(Context context) {
        super(context);
    }

    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setSizeChangedListener(SizeChangedListener sizeChangedListener) {
        this.sizeChangedListener = sizeChangedListener;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (sizeChangedListener != null)
            sizeChangedListener.onSizeChanged(w,h);
    }
}
