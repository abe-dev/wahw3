package com.abedev.wa.homework3;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.ColorMatrix;
import android.support.annotation.ArrayRes;
import android.support.v4.content.ContextCompat;

/**
 * Created by home on 27.08.2016.
 */
public class ColorMatrixResources {
    public static ColorMatrix get(Context context, @ArrayRes int id) {
        TypedArray matrixArray = context.getResources().obtainTypedArray(id);
        if (matrixArray.length() !=20)
            throw new IllegalArgumentException("Not color matrix array!");
        float[] resultArray = new float[20];
        for (int i = 0; i < matrixArray.length(); i++) {
            resultArray[i] = matrixArray.getFloat(i,0);
        }
        matrixArray.recycle();
        return new ColorMatrix(resultArray);
    }
}
