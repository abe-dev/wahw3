package com.abedev.wa.homework3;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.abedev.wa.homework3.MyImageView.SizeChangedListener;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity implements SizeChangedListener {
    private MyImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = (MyImageView) findViewById(R.id.image_view);
        imageView.setSizeChangedListener(this);
    }





    @Override
    public void onSizeChanged(int w, int h) {
        Bitmap imageBitmap = loadImage(w,h);
        if (imageBitmap == null){
            imageView.setImageBitmap(null);
            return;
        }
        float scale = Math.min(h / (float) imageBitmap.getHeight(),w  / (float) imageBitmap.getWidth());

        Paint imagePaint = new Paint();
        imagePaint.setColorFilter(new ColorMatrixColorFilter(ColorMatrixResources.get(this, R.array.invert_colors)));


        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas tempCanvas = new Canvas(bitmap);
        tempCanvas.save();
        tempCanvas.scale(scale,scale);

        tempCanvas.drawBitmap(imageBitmap,0,0,imagePaint);
        imageBitmap.recycle();

        Drawable gradient = ContextCompat.getDrawable(this, R.drawable.gradient);
        gradient.setBounds(0, 0, imageBitmap.getWidth(), imageBitmap.getHeight());
        gradient.draw(tempCanvas);

        Bitmap frameBitmap = Bitmap.createBitmap(imageBitmap.getWidth(), imageBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas frameCanvas = new Canvas(frameBitmap);
        Drawable frameShape = ContextCompat.getDrawable(this, R.drawable.vector_frame);
        frameShape.setBounds(0, 0, imageBitmap.getWidth(), imageBitmap.getHeight());
        frameShape.draw(frameCanvas);
        Paint framePaint = new Paint();
        framePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        tempCanvas.drawBitmap(frameBitmap, 0, 0, framePaint);
        frameBitmap.recycle();


        tempCanvas.restore();
        imageView.setImageBitmap(bitmap);

    }

    @Nullable
    private Bitmap loadImage(int w, int h) {
        InputStream inputStream = null;
        try {
            inputStream = getAssets().open("santorini.png");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(inputStream, null, options);
            inputStream.reset();
            int sampleSize = (int)Math.floor(Math.max(options.outHeight / (float) h,options.outWidth  / (float) w));
            if (sampleSize > 1) {
                options.inSampleSize = sampleSize;
            }
            options.inJustDecodeBounds = false;
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, options);
            return bitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
}
